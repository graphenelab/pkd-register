const merge = require('webpack-merge');
const webpack = require('webpack');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJs = require('uglifyjs-webpack-plugin');

const {config, SOURCES, DIST} = require('./common');

module.exports = merge(config, {
    mode: 'production',
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'postcss-loader',
                    'sass-loader',
                ],
            },
        ],
    },

    // cache: false,
    watch: false,
    profile: true,
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name][hash].css'
        }),

        new UglifyJs({
            uglifyOptions: {
                compress: {warnings: false},
                output: {comments: false}
            }}),

        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        }),

        new CopyWebpackPlugin([{ context: SOURCES, from: 'assets', to: DIST }]),
    ],
});
