const path = require('path');
const merge = require('webpack-merge');

const { HotModuleReplacementPlugin } = require('webpack');

const { config, DIST } = require('./common');

module.exports = merge(config, {
  mode: 'development',
  profile: false,
  devtool: 'inline-source-map',

  output: {
    path: DIST,
    pathinfo: true,
    filename: '[name].dev.js',
    publicPath: '/',
  },

  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          'css-loader',
          'postcss-loader',
          'sass-loader',
        ],
      },
    ],
  },

  plugins: [
    new HotModuleReplacementPlugin()
  ],

  devServer: {
    contentBase: path.resolve(__dirname, '..', 'public'),
    compress: true,
    hot: true,
    noInfo: true,
    historyApiFallback: {
        disableDotRule: true
    },
    overlay: {
      warning: false,
      errors: true,
    },
  },
});
