const path = require('path');
const {DefinePlugin} = require('webpack');
const HtmlPlugin = require('html-webpack-plugin');

const SOURCES = path.resolve(__dirname, '..', 'src');
const DIST = path.resolve(__dirname, '..', 'dist');

const config = {

    context: SOURCES,
    target: 'web',

    entry: {
        app: ['babel-polyfill', 'react-hot-loader/patch', 'whatwg-fetch', './index.js'],
    },

    output: {
        path: DIST,
        filename: '[name][hash].js',
        publicPath: '/',
    },

    resolve: {
        extensions: ['.js'],
        modules: [
            'node_modules',
            SOURCES,
        ],
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                include: /src/,
                use: 'babel-loader',
            },
            {
                test: /\.svg$/i,
                exclude: /node_modules/,
                include: /svg/,
                use: [
                    'babel-loader',
                    {
                        loader: 'react-svg-loader',
                        options: {
                            svgo: {
                                plugins: [
                                    {
                                        removeViewBox: false
                                    },
                                    {
                                        cleanupIDs: false
                                    }
                                ]
                            },
                            jsx: true
                        }
                    }
                ]
            },
            {
                test: /\.(png|jpe?g|gif|svg)$/,
                exclude: /node_modules/,
                include: /img/,
                use:
                    [
                        'file-loader',
                        {
                            loader: 'image-webpack-loader',
                            options: {
                                mozjpeg: {
                                    quality: 75
                                },
                                pngquant: {
                                    quality: "50",
                                    speed: 4
                                },
                                svgo: {
                                    plugins: [
                                        {
                                            removeViewBox: false
                                        },
                                        {
                                            removeEmptyAttrs: false
                                        },
                                        {
                                            cleanupIDs: false
                                        }
                                    ]
                                },
                                gifsicle: {
                                    optimizationLevel: 7,
                                    interlaced: false
                                },
                                optipng: {
                                    enabled: false
                                },
                                // webp: {
                                //     quality: 30
                                // }
                            }
                        }
                    ]
            },
            {test: /\.(woff2?|svg)$/, loader: "url-loader?limit=10000", include: /fonts/,},
            {test: /\.(ttf|eot)$/, loader: "file-loader", include: /fonts/,},
            {test: /\.(mp4|ogv|mov)$/, loader: "file-loader", include: /video/,},
        ],
    },

    plugins: [
        new HtmlPlugin({
            title: 'Graphene Lab',
            template: 'index.html',
            hash: true,
            cache: true
        }),
        new DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(process.env.NODE_ENV),
            },
        })
    ],

    node: {
        console: true,
        fs: 'empty',
        net: 'empty',
        tls: 'empty'
    }
};

module.exports = {
    config,
    DIST,
    SOURCES,
};
