import React, { Component, Fragment } from 'react';
import pack from '../package.json';
import Header from "./components/layout/header";
import ChooseName from "./components/choseName";
import BackupPrivate from "./components/backupPrivate";
import {moveTo} from "./functions/moveTo";
import CompleteRegister from "./components/completeRegister";
import {gaCallback} from "./functions/analyticsCallback";

const steps = ['chosenname', 'backup', 'complete'];

const accountDefaultData = {
    name: '',
    prv: '',
    pub: '',
};

class App extends Component{
    state = {
        interval: false,
        status: false,
        // load: true,
        step: 0,
        account: accountDefaultData,
        coinBase: false,
        eosPayment: false,
        paymentError: false,
    };

    componentDidMount(){
        console.log(`version ${pack.version}`);
    }

    nextStep = () => {
        let {step} = this.state;
        if(step < steps.length) this.setState({step: step + 1}, () => moveTo(steps[step + 1]));
    };

    prevStep = () => {
        const step = this.state.step - 1;
        if(step >= 0) moveTo(steps[step], 200);
        document.getElementById(steps[this.state.step]).classList.add('disable');
        setTimeout(() => this.setState({step}), 400)
    };

    chooseName = (name) => {
        const {account} = this.state;
        gaCallback('Chosen name');
        this.setState({account: {...account, name}}, this.nextStep);
    };

    getKeys = (data) => {
        this.setState({
            account: {...this.state.account, ...data.account}
        }, this.nextStep);
    };

    render(){
        const {step, account} = this.state;
        // if(this.state.load) return <p>Loading</p>;

        return (
            <Fragment>
                <Header/>
                <main>
                    <div className="container">
                        <ChooseName
                            submit={this.chooseName}
                            brightness={step > 0 ? step/10 * 2 : 0}
                        />
                        {step > 0 &&
                            <BackupPrivate
                                submit={this.getKeys}
                                account={account}
                                prev={this.prevStep}
                                brightness={step > 1 ? step/10 * 1.5 : 0}
                            />
                        }
                        {step > 1 &&
                            <CompleteRegister account={account} prev={this.prevStep}/>
                        }
                    </div>
                </main>
                <footer>
                    <div className="container">
                        <span className="copyright">© Graphene Lab<br/>2018-2077</span>
                    </div>
                </footer>
            </Fragment>
        )
    }
}

export default App;
