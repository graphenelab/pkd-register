import React, {Component, Fragment} from 'react';
import {payments} from "../../data/paymentsMethod";
import {Card} from "../helpers/card";
import {generateUUID} from "../../functions/generateUUID";
import {pushAccount} from "../../functions/apiRequest";
import {Loader} from "../helpers/loader";

const resourcesName = {
    cpu: 'CPU',
    net:  'NET',
    liquid: 'liquid balance',
    ramBytes: 'RAM volume'
};

class EosPaymentModal extends Component {
    state = {
        address: false,
        memo: false,
        error: false,
        load: false,
        resources: {
            cpu: {value: '0.1', units: 'EOS'},
            net:  {value: '0.1', units: 'EOS'},
            liquid:  {value: '0.4', units: 'EOS'},
            ramBytes:  {value: '5000', units: 'bytes'}
        }
    };

    payment = () => {
        const {pub, name} = this.props.account;

        const params = {
            accountName: name,
            userUuid: generateUUID(),
            activeKey: pub,
            ownerKey: pub,
            paymentMethod: 'eos'
        };

        this.setState(
            {load: true},
            () => pushAccount(params).then(e => {
                if(e.error) this.setState({error: e.error.message});
                if(e.result){
                    const resources = this.state.resources;
                    const {address, memo, cpu, liquid, net, ramBytes} = e.result;
                    resources['cpu'].value = cpu;
                    resources['liquid'].value = liquid;
                    resources['net'].value = net;
                    resources['ramBytes'].value = ramBytes;
                    this.setState({address, memo, resources, error: false, load: false});
                    this.props.callBack(e);
                }
            })
        )
    };

    render(){
        const {address, memo, error, resources, load} = this.state;
        const {price} = this.props;
        return (
            <Card main title='Payment in EOS'>
                {load && <Loader />}
                <p className="text--lg text--secondary">Please transfer EOS according to the details below.</p>
                <p className="text--lg text--secondary">For the minimum payment you'll receive an account with the following parameters:</p>
                <p className="text">
                    {['cpu','liquid','net','ramBytes'].map(el =>
                        <Fragment>{resourcesName[el]}: {resources[el].value} {resources[el].units}<br/></Fragment>
                    )}
                </p>
                <p className="text--lg text--secondary">You'll be able to use liquid EOS for playing immediately.
                    Part of the EOS amount that exceeds the sum of the minimal payment will be deposited to your liquid balance.</p>
                {address &&
                    <Fragment>
                        <p className="title">Transfer details</p>
                        <p className="text">
                            Minimum payment: <span>{price} {payments.find(el => el.key === 'eos').units}</span><br/>
                            Address: <span>{address}</span><br/>
                            Memo: <span>{memo}</span>
                        </p>
                    </Fragment>
                }
                {error && <p className="text text--danger">Error: <span>{error}</span></p>}
                <p className="text--lg text--danger">IMPORTANT: you have to include memo, otherwise the account won't be created.</p>
                {!address && <button className='btn' onClick={this.payment}>Continue</button>}
            </Card>
        )
    }
}

export default EosPaymentModal;
