import React from 'react';
import {Card} from "../helpers/card";

export const CoinBasePaymentModal = ({callBack = undefined}) => {
    return (
        <Card main title='Payment in CoinBase'>
            <p className="text--lg text--secondary">Please make sure you're paying the exact amount shown on the following page. If you send any less or more, your order cannot be processed. Don't send money from an exchange unless you know what you're doing.</p>
            <p className="text--lg text--secondary">Payment by Cryptocurrency takes 5 to 30 minutes. Once we verify your payment, we will create your EOS account right away.</p>
            <p className="text--lg text--danger">Warning: making a payment through exchanges doesn't guarantee your successful payment at Coinbase Commerce (15 minute window).</p>
            <button className='btn' onClick={callBack}>Continue</button>
        </Card>
    )
};
