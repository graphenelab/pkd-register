import React, {Suspense} from 'react';
import NavLink from "react-router-dom/es/NavLink";
import {BlobEffect} from "./helpers/blob";
import {Logo} from "../svg";
import Menu from "./helpers/menu";

export const NotFound = () => (
    <div className="notfound">
        <header className='compact'>
            <NavLink to='/'><Logo className='logo'/></NavLink>
            <Menu />
        </header>
        <div className="container">
            <span className="notfound__num">404</span>
            <p className="notfound__text heading--banner">Page not founded</p>
            <BlobEffect duration='10s'>
                <NavLink to='/' className="notfound__btn btn">back to home</NavLink>
            </BlobEffect>
        </div>
    </div>
);
