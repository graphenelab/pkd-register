import React, {Component} from 'react';
import {Card} from "./helpers/card";
import Field from "./helpers/field";
import {ExtraCard} from "./helpers/extraCard";
import {Backspace} from "../svg";
import {copyToBuffer} from "../functions/copyToBuffer";
import {breakPoints} from "../data/breakpoints";
import {gaCallback} from "../functions/analyticsCallback";

class CompleteRegister extends Component{
    copyData = () => {
        const {name, prv} = this.props.account;
        gaCallback('Completed registration and proceeded to lobby');
        copyToBuffer('regtools='+JSON.stringify({name, prv}));
    };

    render(){
        const {prev, account} = this.props;
        const {name, prv} = account;
        return (
            <div className="step final" id='complete'>
                <Card main title='Final'>
                    <Backspace className='back' onClick={prev}/>
                    <Field label='Account name' className='small' defaultValue={name} disabled/>
                    <Field label='Private key' className='small' defaultValue={prv} disabled/>
                    <a href='https://game.pokerchained.com/' className="btn" target='_blank' rel='noopener' onClick={this.copyData}>Play now</a>
                    <span className="text final__confirmations">Fast and secure game without additional actions and confirmations</span>
                </Card>
            </div>
        )
    }
}


export default CompleteRegister;
