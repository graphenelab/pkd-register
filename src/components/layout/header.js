import React, {Component} from 'react';

import {Logo} from '../../svg/';

class Header extends Component {
    render(){
        const {device} = this.props;
        return (
            <header className={`${device > 2 ? 'compact' : ''}`}>
                <div className="container">
                    <Logo className='logo'/>
                </div>
            </header>
        )
    }
}

export default Header
