import React, {Component, Fragment} from 'react';
import {Card} from "./helpers/card";
import Field from "./helpers/field";
import {ExtraCard} from "./helpers/extraCard";
import {checkErrors} from "../functions/formValidator";
import {errors} from "../data/errors";
import {generateName} from "../functions/generateName";
import {apiRequest, checkAccount} from "../functions/apiRequest";
import {Loader} from "./helpers/loader";
import {breakPoints} from "../data/breakpoints";
import {Dice} from "../svg";

class ChooseName extends Component{
    state = {
        name: '',
        msg: false,
        load: false
    };

    handleValue = (e) => {
        const name = e.target.value.toLowerCase();

        const checkSymbols = /[-_!"`'#%&,:;<>=@{}~\$\(\)\*\+\/\\\?\[\]\^\|]+/;

        if(name.length > 12 || new RegExp(checkSymbols).test(name)) return;
        let msg = false;

        if(name.length && new RegExp(/^[^a-z]/).test(name)) {
            msg = 'firstletter';
        } else if(!checkErrors.name(name) && name.length){
            msg = 'namelength';
        } else if(new RegExp(/[^a-z1-5]/gi).test(name)){
            msg = 'namecontent';
        }

        this.setState({name, msg});
    };

    generateName = () => { this.setState( {load: true}, () => this.checkName(generateName()) ) };

    checkName = (name) => checkAccount(name).then(res => {
        let msg = res ? 'available' : 'busy';
        this.setState({name, msg, load: false});
        return Boolean(res);
    });

    completeStep = () => {
        const {submit} = this.props;
        this.checkName(this.state.name).then(result => {
            if(result && submit) submit(this.state.name);
        });
    };

    render(){
        const {brightness} = this.props;
        const {name, msg, load} = this.state;
        return (
            <div className="step" id='chosenname'>
                <Card main title='Choose account name' overlay={brightness}>
                    {load && <Loader />}
                    <p className="text--lg text--secondary">Account name must be exactly 12 characters long and consist of lowercase characters (a-z) and digits (1-5) only</p>
                    <Field className='lg-margin small'
                           placeholder='Account name'
                           value={name}
                           success={msg === 'available'}
                           msg={msg && errors[msg]}
                           onChange={this.handleValue}
                    >
                        <button className="link with-action" onClick={this.generateName}>
                            <Dice />
                        </button>
                    </Field>
                    <button className="btn" onClick={this.completeStep} disabled={name.length < 12} >Generate keys</button>
                </Card>
                <ExtraCard overlay={brightness}>
                    <p className="text--lg">
                        Your name is the identifier and address on our network.
                        <br/><br/>
                        <span className='text--danger'>Important!</span> The account name cannot be changed
                    </p>
                </ExtraCard>
            </div>
        )
    }
}


export default ChooseName;
