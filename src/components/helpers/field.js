import React, {Component} from 'react';

class Field extends Component {
    state = {
        tooltip: ''
    };

    handleAction = () => {
        if(this.props.action) {
            const {text, func} = this.props.action;
            func();
            if(text){
                this.setState(
                    {tooltip: text},
                    () => setTimeout(
                        () => this.setState({tooltip: ''}),
                        2000)
                )
            }
        }
    };

    render(){
        const {type = 'text', label, children, className, msg, success = false, disabled = false, action = {text: ''}, ...other} = this.props;
        const {tooltip} = this.state;
        const classNames = [
            className ? ' ' + className : '',
            !success && msg ? ' error' : '',
            success ? ' success' : '',
            disabled ? ' disabled': '',
            action.func ? ' action': ''
        ];
        return(
            <span className={`field__wrapper`.concat(...classNames)} onClick={this.handleAction}>
                {label &&
                    <label className='field__label'>
                        {label}
                        {tooltip ? <span className='text--sm text--success'> ({tooltip})</span> : '' }
                    </label>
                }
                <input type={type} className="field__input" disabled={disabled} {...other}/>
                {msg && <span className='field__hint'>{msg}</span>}
                {children}
            </span>
        )
    }
}

export default Field;
