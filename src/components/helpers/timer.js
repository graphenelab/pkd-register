import React, {Component} from 'react';

class Timer extends Component {
    state = {
        minutes: 0,
        hour: 0,
        interval: false
    };

    componentDidMount(){
        this.timer(this.props);
        const interval = setInterval(
            () => this.timer(this.props),
            30000);
        this.setState({interval});
    }

    timer = ({expire}) => {
        const time = expire ? new Date(expire).getTime() : 0;
        const now = Date.now();
        if(time) {
            const allTime = (time - now)/60000;
            const hours = parseInt(allTime/60);
            const minutes = parseInt((time - now)/60000 - hours*60);
            this.setState({hours, minutes});
        }
    };

    render(){
        const {expire} = this.props;
        const {hours, minutes} = this.state;
        return (
            <span className="price">{hours}h{minutes}m</span>
        )
    }
}

export default Timer;
