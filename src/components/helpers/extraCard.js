import React from 'react';

export const ExtraCard = ({title, children, className, overlay}) => (
    <div className={`card card--secondary ${className ? className : ''}`}>
        <span className="card__overlay" style={{opacity: overlay, zIndex: overlay > 0 ? 10 : -1}}/>
        {title && <p className="title">{title}</p>}
        {children}
    </div>
);
