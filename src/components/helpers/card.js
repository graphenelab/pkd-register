import React from 'react';

export const Card = ({title, children, main, overlay}) => (
    <div className={`card ${main ? 'card--main' : ''}`}>
        <span className="card__overlay" style={{opacity: overlay, zIndex: overlay > 0 ? 10 : -1}}/>
        <h3 className="title">{title}</h3>
        <div className="card__content">
            {children}
        </div>
    </div>
);
