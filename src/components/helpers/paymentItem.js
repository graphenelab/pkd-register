import React from 'react';
import {breakPoints} from "../../data/breakpoints";

export const PaymentItem = ({data, click, disabled, hover}) => {
    if(window.innerWidth < breakPoints.lg && disabled) return <span/>;
    const {title, desc, ico, key} = data;

    return (
        <button key={title} className="coinbase__method" onClick={click} onMouseEnter={!disabled ? () => hover(key) : undefined} disabled={disabled}>
            {ico}
            <span className="coinbase__title">
                {title}
                {desc && <span>({desc})</span>}
            </span>
        </button>
    )
}
