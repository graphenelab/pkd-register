import React from 'react';
import {Check} from "../../svg";

const CheckBox = ({id = '', label = '', value = '', className = '', onChange = e => e.preventDefault(), disabled = false}) => {
    return(
        <label htmlFor={id} className={`checkbox${value ? ' checkbox--selected' : ''}${className ? ` ${className}` : ''}${disabled ? ' disabled' : ''}`}>
            <span className="checkbox__ico">
                <Check />
            </span>
            <input id={id} type="checkbox" defaultChecked={value} onClick={onChange} disabled={disabled}/>
            {label && <span className="checkbox__label">{label}</span>}
        </label>
    )
};

export default CheckBox;
