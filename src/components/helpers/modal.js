import React from 'react';
import {Close} from "../../svg";
import EosPaymentModal from "../modals/eosPayment";
import {CoinBasePaymentModal} from "../modals/coinbasePayment";

const Modal = ({children, close = () => {}}) => (
    <div className="modal modal--eos">
        <button className="modal__close" onClick={close}><Close/></button>
        <div className="modal__body">
            {children}
        </div>
    </div>
);

export const EosModal = ({close, ...props}) => {
    return (
        <Modal close={close}>
            <EosPaymentModal {...props}/>
        </Modal>
    )
};

export const CoinBaseModal = ({close, ...props}) => {
    return (
        <Modal close={close}>
            <CoinBasePaymentModal {...props}/>
        </Modal>
    )
};
