import React from 'react';

export const Loader = ({additionalText}) => (
    <div className='loader__wrapper'>
        <span className='loader'>
            <span/><span/><span/>
        </span>
        {Boolean(additionalText) && additionalText}
    </div>
);
