import React, {Component} from 'react';
import ReCAPTCHA from "react-google-recaptcha";
import eosjs_ecc from "eosjs-ecc/lib/api_common";
import {Card} from "./helpers/card";
import Field from "./helpers/field";
import {ExtraCard} from "./helpers/extraCard";
import {Backspace, Copy} from "../svg";
import CheckBox from "./helpers/checkBox";
import {Loader} from "./helpers/loader";
import {generateUUID} from "../functions/generateUUID";
import {getPrice, getStatus, pushAccount} from "../functions/apiRequest";
import {copyToBuffer} from "../functions/copyToBuffer";
import {breakPoints} from "../data/breakpoints";
import {errors} from "../data/errors";
import {gaCallback} from "../functions/analyticsCallback";
import {captchaKey} from "../data/addresses";

class BackupPrivate extends Component{
    state = {
        check: false,
        load: true,
        error: false,
        prv: '',
        pub: '',
        recaptcha: false
    };

    handleChange = () => {this.setState({check: !this.state.check})};
    handleCaptcha = (value) => { this.setState({recaptcha: value}) };

    componentDidMount() {
        this.generateKey(this.props.name);
    };

    generateKey = (name) => {
        eosjs_ecc.randomKey().then(prv => {
            let pub = eosjs_ecc.privateToPublic(prv);
            this.setState({name, prv, pub, load: false});
        });
    };

    copy = (value) => {
        copyToBuffer(value);
        navigator.clipboard.readText();
    };

    pushAccount = () => {
        const {submit} = this.props;
        const {name} = this.props.account;
        const {pub, prv, recaptcha} = this.state;

        this.setState({load: true}, () => {
                gaCallback('Saved keys backup');
                pushAccount({
                    accountName: name,
                    userUuid: generateUUID(),
                    activeKey: pub,
                    ownerKey: pub,
                    paymentMethod: 'promo',
                    recaptcha
                })
                    .then(res => {
                        if(res) {
                            localStorage.setItem('lastpush', String(Date.now()));
                            const interval = () => {
                                getStatus(res.userUuid).then(status => {
                                    return status !== 'DONE'
                                        ? setTimeout(interval, 1000)
                                        : this.setState(
                                            {load: false, error: !status ? errors.notSent: false},
                                            () => submit({prices: 0, account: {name, pub, prv}})
                                        );
                                });
                            };
                            interval();
                        }
                    })
                    .catch(error => {
                        this.setState({load: false, error: errors.notSent});
                    });
            });
    };

    render(){
        const {check, load, error, prv, pub, recaptcha} = this.state;
        const {prev, brightness, account} = this.props;

        const fields = [
            {label: 'Your account', value: account.name, class: ''},
            {label: 'Private key', value: prv, class: 'prv'},
        ];

        const last = localStorage.getItem('lastpush');

        // const captcha = last ? Date.now() - 300000 < parseInt(last) : false;

        return (
            <div className="step backup" id='backup'>
                <Card main title='Back up your private key' overlay={brightness}>
                    {load && <Loader />}
                    <Backspace className='back' onClick={prev}/>
                    <p className="text--lg">Please make sure you've backed up your private key!</p>
                    {fields.map(el =>
                        <Field key={el.label} label={el.label} className={`small ${el.class}`} defaultValue={el.value} disabled action={{text: 'copied', func: () => this.copy(el.value)}}>
                            <button className="link with-action" onClick={() => this.copy(el.value)}>
                                <Copy />
                            </button>
                        </Field>
                    )}
                    <CheckBox
                        id="understand"
                        label='I understand my account cannot be recovered in case I lose my private key'
                        value={check}
                        onChange={this.handleChange}
                    />
                    {!recaptcha && <ReCAPTCHA className='captcha' sitekey={captchaKey} onChange={this.handleCaptcha} />}
                    <button className="btn" onClick={this.pushAccount} disabled={!check || !recaptcha}>
                        Create account
                        {error &&
                        <span className="btn__hint">{error}</span>
                        }
                    </button>
                </Card>
                <ExtraCard overlay={brightness}>
                    <p className="text--lg">Private key is the only way to access your account - it must be kept secret and stored securely. Please note that we don't store your private key - you're the only person responsible for your account's safety!</p>
                </ExtraCard>
            </div>
        )
    }
}


export default BackupPrivate;
