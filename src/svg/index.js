//logo
export { default as Logo } from './logo.svg';
export { default as EOS } from './eos.svg';
export { default as BTC } from './btc.svg';
export { default as CreditCard } from './card.svg';

//icons
export { default as Backspace } from './back.svg';
export { default as Check } from './check.svg';
export { default as Close } from './close.svg';
export { default as Copy } from './copy.svg';
export { default as Dice } from './dices.svg';

