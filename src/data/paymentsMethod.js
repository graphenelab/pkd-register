import React from "react";
import {BTC, CreditCard, EOS} from "../svg";

export const payments = [
    {key: 'crypto', units: 'USD', title: 'Coinbase', desc: '', ico: <BTC className="coinbase__ico"/>},
    {key: 'eos', units: 'EOS', title: 'EOS', desc: '', ico: <EOS className="coinbase__ico"/>},
    {key: 'card', units: 'USD', title: 'Card', desc: 'coming soon', ico: <CreditCard className="coinbase__ico"/>}
];
