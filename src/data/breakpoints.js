export const breakPoints = {
    xs: 350,
    sm: 600,
    md: 900,
    lg: 1200,
    xl: 1600
};
