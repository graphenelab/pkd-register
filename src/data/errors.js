export const errors = {
    firstletter: 'First character must be a letter',
    namelength: 'Name must be 12 characters long',
    namecontent: 'Name must consist of lowercase characters (a-z) and digits (1-5)',
    available: 'This name is available',
    busy: 'This name is already taken',
    notSent: 'Account information not sent'
};
