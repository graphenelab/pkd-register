export const generateName = () => {
    const possible = 'abcdefghijklmnopqrstuvwxyz12345';
    let name = '';

    for (let i = 0; i < 7; i++) {
        name += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return `poker${name}`;
};
