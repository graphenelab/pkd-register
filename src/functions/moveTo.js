export const moveTo = (anchor, delta = 0) => {
    const elem = document.getElementById(anchor);
    window.scrollTo({
        'behavior': 'smooth',
        'left': 0,
        'top': elem.offsetTop - delta
    });
};
