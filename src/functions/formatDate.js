import React from "react";

const months = ['Jun', 'Feb', 'Mar', 'Apr', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

export const formatDate = (date) => {
    const temp = new Date(date);
    return <span className="date">{months[temp.getMonth()]}&nbsp;{temp.getDay()}{`\n`}{temp.getFullYear()}</span>
};
