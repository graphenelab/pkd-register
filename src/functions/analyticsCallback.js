import * as ReactGA from "react-ga";

export const gaCallback = (action) => {
    ReactGA.event({ category: 'Step', action });
};
