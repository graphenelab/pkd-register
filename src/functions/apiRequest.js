import {link} from "../data/addresses";

export const apiRequest = async (method, params) => {
    // const body = JSON.stringify({
    //     id: "json",
    //     jsonrpc: "2.0",
    //     method,
    //     params
    // });
    return await fetch(`${link}/${method}`, {
        method: 'POST',
        body: JSON.stringify(params),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    })
        .then(response => {
            if(response.status === 400){
                throw new Error();
                return;
            }
            return response.json()
        })
        .then((e) => e)
        // .catch(console.error);
};

export const checkAccount = (account) => apiRequest('isAccountAvailable', {account});
export const pushAccount = (params) => apiRequest('purchaseAccount', params);
export const getPrice = () => apiRequest('priceAccount');
export const getStatus = (userUuid) => apiRequest('purchaseStatus', {userUuid});
