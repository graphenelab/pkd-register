export const checkErrors = {
    name: val => val.trim().length === 12,
    email: val => new RegExp(/[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm).test(val),
    msg: val => val.trim().length > 10
};
