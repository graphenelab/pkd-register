import React from 'react'
import ReactDom from 'react-dom'

import './styles/styles.scss'
import App from './App'
import {en} from "./translations";
import counterpart from "counterpart";
import * as ReactGA from "react-ga";

counterpart.registerTranslations('EN', en);

let selectedLang = localStorage.getItem('language');

if(!selectedLang) selectedLang = 'EN';

counterpart.setLocale(selectedLang);

ReactGA.initialize('UA-136747315-5');
ReactGA.pageview(window.location.pathname + window.location.search);

ReactDom.render(
    <App/>,
    document.getElementById('root'),
);
